﻿===== InitialEmptyJson =====
GET /movies
Accept: application/json
----- REQUEST:
----- RESPONSE:
{"movie":[]}

===== PostJson =====
POST /movies
Accept: application/json
----- REQUEST:
{"title":"Batman Begins","year":2005,"director":"Christopher Nolan","actor":["Christian Bale"]}
----- RESPONSE:
{"id":1}
-----
POST /movies
Accept: application/json
----- REQUEST:
{"title":"The Dark Knight","year":2008,"director":"Christopher Nolan","actor":["Christian Bale","Heath Ledger"]}
----- RESPONSE:
{"id":2}

===== PutInsertJson =====
PUT /movies/3
Accept: application/json
----- REQUEST:
{"title":"American Sniper","year":2014,"director":"Clint Eastwood"}
----- RESPONSE:
-----
GET /movies/3
Accept: application/json
----- REQUEST:
----- RESPONSE:
{"title":"American Sniper","year":2014,"director":"Clint Eastwood","actor":[]}

===== PutUpdateJson =====
PUT /movies/3
Accept: application/json
----- REQUEST:
{"title":"American Sniper","year":2014,"director":"Clint Eastwood","actor":["Bradley Cooper","Kyle Gallner"]}
----- RESPONSE:
-----
GET /movies/3
Accept: application/json
----- REQUEST:
----- RESPONSE:
{"title":"American Sniper","year":2014,"director":"Clint Eastwood","actor":["Bradley Cooper","Kyle Gallner"]}

===== GetJson =====
GET /movies/3
Accept: application/json
----- REQUEST:
----- RESPONSE:
{"title":"American Sniper","year":2014,"director":"Clint Eastwood","actor":["Bradley Cooper","Kyle Gallner"]}

===== Get404Json =====
GET /movies/4
Accept: application/json

HTTP/1.1 404 Not Found
----- REQUEST:
----- RESPONSE:

===== GetAllJson =====
GET /movies
Accept: application/json
----- REQUEST:
----- RESPONSE:
{"movie":[{"title":"The Dark Knight","year":2008,"director":"Christopher Nolan","actor":["Christian Bale","Heath Ledger"]},{"title":"American Sniper","year":2014,"director":"Clint Eastwood","actor":["Bradley Cooper","Kyle Gallner"]},{"title":"Batman Begins","year":2005,"director":"Christopher Nolan","actor":["Christian Bale"]}]}

===== DeleteJson =====
DELETE /movies/3
Accept: application/json
----- REQUEST:
----- RESPONSE:

===== QueryByDirectorJson =====
PUT /movies/5
Accept: application/json
----- REQUEST:
{"title":"The Hunger Games: Catching Fire","year":2013,"director":"Francis Lawrence","actor":["Jennifer Lawrence"]}
----- RESPONSE:
-----
PUT /movies/6
Accept: application/json
----- REQUEST:
{"title":"Elysium","year":2013,"director":"Neill Blomkamp","actor":["Matt Daemon"]}
----- RESPONSE:
-----
PUT /movies/7
Accept: application/json
----- REQUEST:
{"title":"Frozen","year":2013,"director":"Chris Buck"}
----- RESPONSE:
-----
GET /movies/find?year=2013&orderby=Director
Accept: application/json
----- REQUEST:
----- RESPONSE:
{"id":[7,5,6]}
-----
DELETE /movies/5
Accept: application/json
----- REQUEST:
----- RESPONSE:
-----
DELETE /movies/6
Accept: application/json
----- REQUEST:
----- RESPONSE:
-----
DELETE /movies/7
Accept: application/json
----- REQUEST:
----- RESPONSE:

===== QueryByTitleJson =====
PUT /movies/8
Accept: application/json
----- REQUEST:
{"title":"The Hunger Games: Catching Fire","year":2013,"director":"Francis Lawrence","actor":["Jennifer Lawrence"]}
----- RESPONSE:
-----
PUT /movies/9
Accept: application/json
----- REQUEST:
{"title":"Elysium","year":2013,"director":"Neill Blomkamp","actor":["Matt Daemon"]}
----- RESPONSE:
-----
PUT /movies/10
Accept: application/json
----- REQUEST:
{"title":"Frozen","year":2013,"director":"Chris Buck"}
----- RESPONSE:
-----
GET /movies/find?year=2013&orderby=Title
Accept: application/json
----- REQUEST:
----- RESPONSE:
{"id":[9,10,8]}
-----
DELETE /movies/8
Accept: application/json
----- REQUEST:
----- RESPONSE:
-----
DELETE /movies/9
Accept: application/json
----- REQUEST:
----- RESPONSE:
-----
DELETE /movies/10
Accept: application/json
----- REQUEST:
----- RESPONSE:

===== Clearing database =====
DELETE /movies/1
Accept: application/json
----- REQUEST:
----- RESPONSE:
-----
DELETE /movies/2
Accept: application/json
----- REQUEST:
----- RESPONSE:
-----
