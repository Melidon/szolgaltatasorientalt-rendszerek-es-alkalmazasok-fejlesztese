// Import express:
import express, { NextFunction, Request, Response } from "express";
import { Hello } from "../schemas/hello";
import { NotFound } from "http-errors";

// Create a new express router:
const router = express.Router();

interface RequestParams {}

interface ResponseBody {}

interface RequestBody {}

// Specify that 'name' is a query parameter of type 'string':
interface RequestQuery {
  name: string;
}

/* GET request: */
router.get(
  "/",
  async (
    req: Request<RequestParams, ResponseBody, RequestBody, RequestQuery>,
    res: Response,
    next: NextFunction
  ) => {
    // Get the 'name' query param:
    const name = req.query.name;
    const hello = await Hello.findOne({ name: name }).exec();
    if (hello) {
      // Send response:
      res.send(hello.message);
    } else {
      return next(NotFound());
    }
  }
);

// Export the router:
export default router;
