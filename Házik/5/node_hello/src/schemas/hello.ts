import mongoose, { Schema, Document } from "mongoose";
import { IHello } from "../interfaces/hello";

const HelloSchema = new Schema<IHello & Document>({
  name: String,
  message: String,
});

export const Hello = mongoose.model("Hello", HelloSchema, "Hello");
