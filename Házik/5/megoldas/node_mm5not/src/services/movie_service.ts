import express, { NextFunction, Request, Response } from "express";
import { Movie } from "../schemas/movie";
import { NotFound, InternalServerError } from "http-errors";
import { IMovie, IMovieId, IMovieIdList, IMovieList } from "../interfaces/movie";

const router = express.Router();

interface IdParam {
  id: string;
}
interface EmptyRequestParams {}
interface EmptyResponseBody {}
interface EmptyRequestBody {}
interface EmptyRequestQuery {}
interface FindRequestQuery {
  year: number;
  orderby: string;
}

// GET /movies
router.get("/", async (req: Request<EmptyRequestParams, IMovieList, EmptyRequestBody, EmptyRequestQuery>, res: Response<IMovieList>, next: NextFunction) => {
  try {
    const movies = await Movie.find().exec();
    const movieList: IMovieList = { movie: movies };
    res.send(movieList);
  } catch (error) {
    next(InternalServerError());
  }
});

// GET /movies/find?year={year}&orderby={field}
router.get("/find", async (req: Request<EmptyRequestParams, IMovieIdList, EmptyRequestBody, FindRequestQuery>, res: Response<IMovieIdList>, next: NextFunction) => {
  const year = req.query.year;
  const orderby = req.query.orderby.toLocaleLowerCase();
  try {
    const movies = await Movie.find({ year: year }).sort(orderby).exec();
    const movieList: IMovieIdList = { id: movies.map((movie) => movie._id) };
    res.send(movieList);
  } catch (error) {
    next(InternalServerError());
  }
});

// GET /movies/{id}
router.get("/:id", async (req: Request<IdParam, IMovie, EmptyRequestBody, EmptyRequestQuery>, res: Response<IMovie>, next: NextFunction) => {
  const id = req.params.id;
  try {
    const movie = await Movie.findById(id).exec();
    if (movie) {
      res.send(movie);
    } else {
      next(NotFound("Movie not found"));
    }
  } catch (error) {
    next(InternalServerError());
  }
});

// POST /movies
router.post("/", async (req: Request<EmptyRequestParams, IMovieId, IMovie, EmptyRequestQuery>, res: Response<IMovieId>, next: NextFunction) => {
  const movie = req.body;
  try {
    const createdMovie = await Movie.create(movie);
    const movieId: IMovieId = { id: createdMovie._id };
    res.send(movieId);
  } catch (error) {
    next(InternalServerError());
  }
});

// PUT /movies/{id}
router.put("/:id", async (req: Request<IdParam, EmptyResponseBody, IMovie, EmptyRequestQuery>, res: Response<EmptyResponseBody>, next: NextFunction) => {
  const id = req.params.id;
  const movieData = req.body;
  try {
    await Movie.replaceOne({ _id: id }, movieData, { upsert: true }).exec();
    res.send();
  } catch (error) {
    next(InternalServerError());
  }
});

// DELETE /movies/{id}
router.delete("/:id", async (req: Request<IdParam, EmptyResponseBody, EmptyRequestBody, EmptyRequestQuery>, res: Response<EmptyResponseBody>, next: NextFunction) => {
  const id = req.params.id;
  try {
    const movie = await Movie.findByIdAndDelete(id).exec();
    if (movie) {
      res.send();
    } else {
      next(NotFound("Movie not found"));
    }
  } catch (error) {
    next(InternalServerError());
  }
});

export default router;
