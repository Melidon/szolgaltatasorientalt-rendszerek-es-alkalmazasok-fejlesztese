import { Document, Schema, model } from "mongoose";
import { IMovie } from "../interfaces/movie";
import { InternalServerError } from "http-errors";

const MovieSchema = new Schema<IMovie & Document>({
  _id: Number,
  title: { type: String, required: true },
  year: { type: Number, required: true },
  director: { type: String, required: true },
  actor: { type: [String], required: true },
});

MovieSchema.pre<IMovie & Document>("save", async function (next) {
  const currentMovie = this;
  if (!currentMovie._id) {
    try {
      const highestMovie = await Movie.findOne({}, "_id").sort("-_id").exec();
      currentMovie._id = highestMovie ? highestMovie._id + 1 : 1;
    } catch (error) {
      next(InternalServerError());
    }
  }
  next();
});

MovieSchema.set("toJSON", {
  transform: function (doc, ret) {
    delete ret._id;
    delete ret.__v;
  },
});

export const Movie = model("Movie", MovieSchema, "Movies");
