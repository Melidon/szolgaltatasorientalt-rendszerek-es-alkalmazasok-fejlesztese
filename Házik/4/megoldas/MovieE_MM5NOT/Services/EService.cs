using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Streaming;

namespace MovieE_MM5NOT.Services;

public class EService : SingleMovie.SingleMovieBase
{
    private const string e = "27182818284590452353602874713526624977572470936999595749669676277240766303535475945713821785251664274274663919320030599218174135966290435729003342952605956307381323286279434907632338298807531952510190115738341879307021540891499348841675092447614606680822648001684774118537423454424371075390777449920695517027618386062613313845830007520449338265602976067371132007093287091274437470472306969772093101416928368190255151086574637721112523897844250569536967707854499699679468644549059879316368892300987931";
    private static readonly IEnumerable<int> digits = e.Select(digit => Convert.ToInt32(digit) - Convert.ToInt32('0')).ToArray();

    public override Task<GetTitleReply> GetTitle(GetTitleRequest request, ServerCallContext context)
    {
        var getTitleReply = new GetTitleReply { Title = "e" };
        return Task.FromResult(getTitleReply);
    }

    public override Task<GetLengthReply> GetLength(GetLengthRequest request, ServerCallContext context)
    {
        var getLengthReply = new GetLengthReply { Length = e.Length };
        return Task.FromResult(getLengthReply);
    }

    public override Task<GetFramesReply> GetFrames(GetFramesRequest request, ServerCallContext context)
    {
        var getFramesReply = new GetFramesReply();
        getFramesReply.Frame.AddRange(digits);
        return Task.FromResult(getFramesReply);
    }
}
