using System;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieStream_MM5NOT.Services;
using Streaming;

var builder = WebApplication.CreateBuilder(args);

// Additional configuration is required to successfully run gRPC on macOS.
// For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682

// Add services to the container.
builder.Services.AddGrpc();

var singleMovieUrls = builder.Configuration.GetSection("SingleMovies:Url").Get<List<string>>();
singleMovieUrls?.ForEach(movieUrl =>
{
    builder.Services.AddGrpcClient<SingleMovie.SingleMovieClient>(
        name: movieUrl,
        configureClient: grpcClientFactoryOptions =>
        {
            grpcClientFactoryOptions.Address = new Uri(movieUrl);
        }
    ).ConfigurePrimaryHttpMessageHandler(
        configureHandler: () =>
        {
            var handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
            return handler;
        }
    );
});

var app = builder.Build();

// Configure the HTTP request pipeline.
app.MapGrpcService<IrrationalStream>();
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.Run();
