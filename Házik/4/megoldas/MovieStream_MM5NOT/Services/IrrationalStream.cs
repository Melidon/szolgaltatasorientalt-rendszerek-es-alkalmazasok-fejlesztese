using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Streaming;

namespace MovieStream_MM5NOT.Services;

public class IrrationalStream : MovieStream.MovieStreamBase
{
    private readonly IEnumerable<SingleMovie.SingleMovieClient> _singleMovieClients;

    public IrrationalStream(IEnumerable<SingleMovie.SingleMovieClient> singleMovieClients)
    {
        _singleMovieClients = singleMovieClients;
    }

    public override async Task<GetMoviesReply> GetMovies(GetMoviesRequest request, ServerCallContext context)
    {
        var movieInfoTasks = _singleMovieClients.Select(async singleMovieClient =>
        {
            var getTitleReply = await singleMovieClient.GetTitleAsync(new GetTitleRequest());
            var getLengthReply = await singleMovieClient.GetLengthAsync(new GetLengthRequest());
            return new MovieInfo
            {
                Title = getTitleReply.Title,
                Length = getLengthReply.Length,
            };
        });
        var movieInfos = await Task.WhenAll(movieInfoTasks);
        var getMoviesReply = new GetMoviesReply();
        getMoviesReply.Movies.AddRange(movieInfos);
        return getMoviesReply;
    }

    public override async Task Watch(WatchRequest request, IServerStreamWriter<WatchReply> responseStream, ServerCallContext context)
    {
        var singleMovieClientTasks = _singleMovieClients.Select(async singleMovieClient =>
        {
            var getTitleReply = await singleMovieClient.GetTitleAsync(new GetTitleRequest());
            return getTitleReply.Title == request.Title ? singleMovieClient : null;
        });
        var singleMovieClient = (await Task.WhenAll(singleMovieClientTasks)).SingleOrDefault(singleMovieClient => singleMovieClient is not null);
        if (singleMovieClient is null)
        {
            throw new RpcException(new Status(StatusCode.FailedPrecondition, $"Movie not found with title: {request.Title}"));
        }
        var getLengthReply = await singleMovieClient.GetLengthAsync(new GetLengthRequest());
        if (request.StartPosition < 0 || request.StartPosition > getLengthReply.Length)
        {
            throw new RpcException(new Status(StatusCode.OutOfRange, $"The requested start position ({request.StartPosition}) is invalid. The movie '{request.Title}' has a length of {getLengthReply.Length}."));
        }
        var getFramesReply = await singleMovieClient.GetFramesAsync(new GetFramesRequest());
        foreach (var frame in getFramesReply.Frame.Skip(request.StartPosition))
        {
            await responseStream.WriteAsync(new WatchReply { Frame = frame });
        }
    }
}
