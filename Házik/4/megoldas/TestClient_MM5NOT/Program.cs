﻿using System;
using System.Linq;
using System.Net.Http;
using Grpc.Core;
using Grpc.Net.Client;
using Streaming;

Console.WriteLine("Getting movies...");

using var httpHandler = new HttpClientHandler();
httpHandler.ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
using var channel = GrpcChannel.ForAddress("https://localhost:5000", new GrpcChannelOptions { HttpHandler = httpHandler });
var movieStreamClient = new MovieStream.MovieStreamClient(channel);
var getMoviesReply = await movieStreamClient.GetMoviesAsync(new GetMoviesRequest());

Console.WriteLine("Available movies:");
foreach (var movie in getMoviesReply.Movies)
{
    Console.WriteLine(movie.ToString());
}

Console.WriteLine("Select a movie: ");
string? title;
bool first = true;
do
{
    if (!first)
    {
        Console.WriteLine("That's not a valid title.");
    }
    first = false;
    title = Console.ReadLine();
} while (!getMoviesReply.Movies.Select(movieInfo => movieInfo.Title).Contains(title));

Console.WriteLine("Select start position: ");
int startPosition;
var length = getMoviesReply.Movies.Where(movieInfo => movieInfo.Title == title).Single().Length;
string? line;
first = true;
do
{
    if (!first)
    {
        Console.WriteLine("That's not a valid start position.");
    }
    first = false;
    line = Console.ReadLine();
} while (!int.TryParse(line, out startPosition) || startPosition < 0 || startPosition > length);

Console.WriteLine("Starting stream...");
var stream = movieStreamClient.Watch(new WatchRequest
{
    Title = title,
    StartPosition = startPosition,
});

await foreach (var watchReply in stream.ResponseStream.ReadAllAsync())
{
    Console.Write(watchReply.Frame);
}

Console.WriteLine();
