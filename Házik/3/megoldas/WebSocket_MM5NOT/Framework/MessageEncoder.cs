using System.Net.WebSockets;
using System.Text;
using WebSocket_MM5NOT.Extensions;
using WebSocket_MM5NOT.Messages;

namespace WebSocket_MM5NOT.Framework;

public static class MessageEncoder
{
    public static async Task<(WebSocketReceiveResult, IncomingMessage?)> ReceiveAsync(WebSocket socket)
    {
        var buffer = new byte[1024 * 4];
        var result = await socket.ReceiveAsync(
            buffer: new ArraySegment<byte>(buffer),
            cancellationToken: CancellationToken.None
        );
        if (result.MessageType == WebSocketMessageType.Text)
        {
            var text = Encoding.UTF8.GetString(buffer, 0, result.Count);
            return (result, text.ToIncomingMessage());
        }
        return (result, null);
    }

    public static async Task SendAsync(WebSocket socket, OutgoingMessage outgoingMessage)
    {
        var message = outgoingMessage.FromOutgoingMessage();
        var buffer = new ArraySegment<byte>(Encoding.ASCII.GetBytes(message), 0, message.Length);
        await socket.SendAsync(
            buffer: buffer,
            messageType: WebSocketMessageType.Text,
            endOfMessage: true,
            cancellationToken: CancellationToken.None
        );
    }
}
