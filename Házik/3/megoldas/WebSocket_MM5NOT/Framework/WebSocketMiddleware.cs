using System.Net.WebSockets;
using WebSocket_MM5NOT.Endpoints;

namespace WebSocket_MM5NOT.Framework;

public class WebSocketMiddleware
{
    // TODO: Remove this!
    private readonly RequestDelegate _next;
    private readonly IEndpoint _server;

    public WebSocketMiddleware(RequestDelegate next, IEndpoint server)
    {
        _next = next;
        _server = server;
    }

    public async Task Invoke(HttpContext context)
    {
        if (!context.WebSockets.IsWebSocketRequest)
        {
            return;
        }
        var socket = await context.WebSockets.AcceptWebSocketAsync();
        await _server.OpenAsync(socket);
        try
        {
            while (socket.State == WebSocketState.Open)
            {
                await HandleMessage(socket);
            }
        }
        catch (Exception ex)
        {
            await _server.CloseAsync(socket);
            await socket.CloseAsync(WebSocketCloseStatus.InternalServerError, ex.Message, CancellationToken.None);
            throw;
        }
    }

    private async Task HandleMessage(WebSocket socket)
    {
        var (result, incomingMessage) = await MessageEncoder.ReceiveAsync(socket);
        if (incomingMessage is not null)
        {
            var outgoingMessage = await _server.MessageAsync(socket, incomingMessage);
            if (outgoingMessage is not null)
            {
                await MessageEncoder.SendAsync(socket, outgoingMessage);
            }
        }
        else if (result.MessageType == WebSocketMessageType.Close)
        {
            await _server.CloseAsync(socket);
            await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, null, CancellationToken.None);
        }
    }
}
