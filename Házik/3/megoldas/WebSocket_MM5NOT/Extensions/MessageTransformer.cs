using System.Text.Json;
using System.Text.Json.Serialization;
using WebSocket_MM5NOT.Messages;

namespace WebSocket_MM5NOT.Extensions;

public static class MessageTransformer
{
    private static readonly JsonSerializerOptions _options = new JsonSerializerOptions
    {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        Converters = {
            new JsonStringEnumConverter(JsonNamingPolicy.CamelCase),
        },
    };

    public static IncomingMessage? ToIncomingMessage(this string message)
    {
        return JsonSerializer.Deserialize<IncomingMessage>(message, _options);
    }

    public static string FromOutgoingMessage(this OutgoingMessage message)
    {
        return JsonSerializer.Serialize(message, _options);
    }
}
