namespace WebSocket_MM5NOT.Extensions;

public static class StringExtensions
{
    public static string ToCamelCase(this string str)
    {
        if (char.IsUpper(str[0]))
        {
            str = char.ToLower(str[0]) + str.Substring(1);
        }
        return str;
    }

}
