using System.Text.Json.Serialization;
using WebSocket_MM5NOT.Extensions;

namespace WebSocket_MM5NOT.Messages;

[JsonConverter(typeof(OutgoingMessageConverter))]
public abstract class OutgoingMessage
{
    public abstract string Type { get; }
}

public class RoomSize : OutgoingMessage
{
    public override string Type => nameof(RoomSize).ToCamelCase();
    public int Rows { get; init; }
    public int Columns { get; init; }
}

public class SeatStatus : OutgoingMessage
{
    public override string Type => nameof(SeatStatus).ToCamelCase();
    public int Row { get; init; }
    public int Column { get; init; }
    public SeatStatusEnum Status { get; init; } = SeatStatusEnum.Free;
}

public class LockResult : OutgoingMessage
{
    public override string Type => nameof(LockResult).ToCamelCase();
    public Guid LockId { get; init; }
}

public class Error : OutgoingMessage
{
    public override string Type => nameof(Error).ToCamelCase();
    public string Message { get; init; } = string.Empty;
}

public enum SeatStatusEnum
{
    Free,
    Locked,
    Reserved,
}
