using System.Text.Json.Serialization;
using WebSocket_MM5NOT.Extensions;

namespace WebSocket_MM5NOT.Messages;

[JsonConverter(typeof(IncomingMessageConverter))]
public abstract class IncomingMessage
{
    public abstract string Type { get; }
}

public class InitRoom : IncomingMessage
{
    public override string Type => nameof(InitRoom).ToCamelCase();
    public int Rows { get; init; }
    public int Columns { get; init; }
}

public class GetRoomSize : IncomingMessage
{
    public override string Type => nameof(GetRoomSize).ToCamelCase();
}

public class UpdateSeats : IncomingMessage
{
    public override string Type => nameof(UpdateSeats).ToCamelCase();
}

public class LockSeat : IncomingMessage
{
    public override string Type => nameof(LockSeat).ToCamelCase();
    public int Row { get; init; }
    public int Column { get; init; }
    public Guid LockId { get; init; }
}

public class UnlockSeat : IncomingMessage
{
    public override string Type => nameof(UnlockSeat).ToCamelCase();
    public Guid LockId { get; init; }
}

public class ReserveSeat : IncomingMessage
{
    public override string Type => nameof(ReserveSeat).ToCamelCase();
    public Guid LockId { get; init; }
}
