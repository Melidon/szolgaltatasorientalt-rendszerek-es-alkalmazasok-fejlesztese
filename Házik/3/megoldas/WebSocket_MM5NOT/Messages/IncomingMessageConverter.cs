using System.Text.Json;
using System.Text.Json.Serialization;

namespace WebSocket_MM5NOT.Messages;

public class IncomingMessageConverter : JsonConverter<IncomingMessage>
{
    public override IncomingMessage? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        using var document = JsonDocument.ParseValue(ref reader);

        if (!document.RootElement.TryGetProperty("type", out var typeProperty))
        {
            throw new JsonException("Missing type property");
        }

        var incomingMessageType = typeProperty.GetString() switch
        {
            "initRoom" => typeof(InitRoom),
            "getRoomSize" => typeof(GetRoomSize),
            "updateSeats" => typeof(UpdateSeats),
            "lockSeat" => typeof(LockSeat),
            "unlockSeat" => typeof(UnlockSeat),
            "reserveSeat" => typeof(ReserveSeat),
            _ => throw new JsonException($"Unknown incoming message type: {typeProperty.GetString()}")
        };

        var incomingMessage = JsonSerializer.Deserialize(document, incomingMessageType, options) as IncomingMessage;
        return incomingMessage;
    }

    public override void Write(Utf8JsonWriter writer, IncomingMessage value, JsonSerializerOptions options)
    {
        throw new NotImplementedException();
    }
}
