using System.Text.Json;
using System.Text.Json.Serialization;

namespace WebSocket_MM5NOT.Messages;

public class OutgoingMessageConverter : JsonConverter<OutgoingMessage>
{
    public override bool CanConvert(Type typeToConvert)
    {
        return typeof(OutgoingMessage).IsAssignableFrom(typeToConvert);
    }

    public override OutgoingMessage Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        throw new NotImplementedException();
    }

    public override void Write(Utf8JsonWriter writer, OutgoingMessage value, JsonSerializerOptions options)
    {
        JsonSerializer.Serialize(writer, value, value.GetType(), options);
    }

}
