using WebSocket_MM5NOT.Endpoints;
using WebSocket_MM5NOT.Framework;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<IEndpoint, CinemaEndpoint>();

var app = builder.Build();

app.UsePathBase("/WebSocket_MM5NOT");

app.UseStaticFiles();
app.UseWebSockets();
app.UseMiddleware<WebSocketMiddleware>();

app.Run();
