using System.Net.WebSockets;
using WebSocket_MM5NOT.Messages;

namespace WebSocket_MM5NOT.Endpoints;

public interface IEndpoint
{
    Task OpenAsync(WebSocket socket);
    Task CloseAsync(WebSocket socket);
    Task ErrorAsync(WebSocket socket, Exception Exception);
    Task<OutgoingMessage?> MessageAsync(WebSocket socket, IncomingMessage incomingMessage);
}
