using System.Net.WebSockets;
using WebSocket_MM5NOT.Framework;
using WebSocket_MM5NOT.Messages;

namespace WebSocket_MM5NOT.Endpoints;

using Position = Tuple<int, int>;

public class CinemaEndpoint : IEndpoint
{
    private readonly HashSet<WebSocket> _sessions = new();
    private Position _size = Tuple.Create(0, 0);
    private readonly Dictionary<Position, SeatStatusEnum> _seats = new();
    private readonly Dictionary<Guid, Position> _locks = new();

    public Task OpenAsync(WebSocket socket)
    {
        Console.WriteLine("WebSocket opened.");

        _sessions.Add(socket);

        return Task.CompletedTask;
    }

    public Task CloseAsync(WebSocket socket)
    {
        Console.WriteLine("WebSocket closed.");

        _sessions.Remove(socket);

        return Task.CompletedTask;
    }

    public Task ErrorAsync(WebSocket socket, Exception exception)
    {
        Console.WriteLine("WebSocket error: " + exception.Message);

        return Task.CompletedTask;
    }

    public async Task<OutgoingMessage?> MessageAsync(WebSocket socket, IncomingMessage incomingMessage)
    {
        Console.WriteLine($"WebSocket incoming message: {incomingMessage}");

        var outgoingMessage = incomingMessage switch
        {
            InitRoom initRoom => InitRoom(initRoom),
            GetRoomSize getRoomSize => GetRoomSize(),
            UpdateSeats updateSeats => await UpdateSeatsAsync(socket),
            LockSeat lockSeat => await LockSeat(lockSeat, socket),
            UnlockSeat unlockSeat => await UnlockSeat(unlockSeat),
            ReserveSeat reserveSeat => await ReserveSeat(reserveSeat),
            _ => null,
        };

        return outgoingMessage;
    }

    private OutgoingMessage? InitRoom(InitRoom initRoom)
    {
        if (initRoom.Rows <= 0 || initRoom.Columns <= 0)
        {
            var error = new Error
            {
                Message = "The number of rows and columns must be a positive integer."
            };
            return error;
        }
        _size = Tuple.Create(initRoom.Rows, initRoom.Columns);
        _seats.Clear();
        _locks.Clear();
        foreach (var row in Enumerable.Range(0, initRoom.Rows))
        {
            foreach (var column in Enumerable.Range(0, initRoom.Columns))
            {
                _seats.Add(Tuple.Create(row, column), SeatStatusEnum.Free);
            }
        }
        return null;
    }

    private OutgoingMessage? GetRoomSize()
    {
        var (rows, columns) = _size;
        return new RoomSize
        {
            Rows = rows,
            Columns = columns,
        };
    }

    private async Task<OutgoingMessage?> UpdateSeatsAsync(WebSocket socket)
    {
        var tasks = _seats.Select(seat =>
        {
            var ((row, column), status) = seat;
            return MessageEncoder.SendAsync(socket, new SeatStatus
            {
                Row = row,
                Column = column,
                Status = status,
            });
        });
        await Task.WhenAll(tasks);
        return null;
    }

    private async Task<OutgoingMessage?> LockSeat(LockSeat lockSeat, WebSocket socket)
    {
        var position = Tuple.Create(lockSeat.Row, lockSeat.Column);
        if (_seats.TryGetValue(position, out var status) && status == SeatStatusEnum.Free)
        {
            var lockId = Guid.NewGuid();
            _locks.Add(lockId, position);
            await SetSeatStatusAsync(position, SeatStatusEnum.Locked);
            var outgoingMessage = new LockResult
            {
                LockId = lockId,
            };
            return outgoingMessage;
        }
        else
        {
            var outgoingMessage = new Error
            {
                Message = $"The seat at row {lockSeat.Row} and column {lockSeat.Column} is either not free or out of range.",
            };
            return outgoingMessage;
        }
    }

    private async Task<OutgoingMessage?> UnlockSeat(UnlockSeat unlockSeat)
    {
        if (_locks.TryGetValue(unlockSeat.LockId, out var position))
        {
            _locks.Remove(unlockSeat.LockId);
            await SetSeatStatusAsync(position, SeatStatusEnum.Free);
            return null;
        }
        else
        {
            var outgoingMessage = new Error
            {
                Message = $"The provided lock ID '{unlockSeat.LockId}' is invalid.",
            };
            return outgoingMessage;
        }
    }

    private async Task<OutgoingMessage?> ReserveSeat(ReserveSeat reserveSeat)
    {
        if (_locks.TryGetValue(reserveSeat.LockId, out var position))
        {
            _locks.Remove(reserveSeat.LockId);
            await SetSeatStatusAsync(position, SeatStatusEnum.Reserved);
            return null;
        }
        else
        {
            var outgoingMessage = new Error
            {
                Message = $"The provided lock ID '{reserveSeat.LockId}' is invalid.",
            };
            return outgoingMessage;
        }
    }

    private Task SetSeatStatusAsync(Position position, SeatStatusEnum status)
    {
        _seats[position] = status;
        var (row, column) = position;
        var outgoingMessage = new SeatStatus
        {
            Row = row,
            Column = column,
            Status = status,
        };
        var tasks = _sessions.Select(socket => MessageEncoder.SendAsync(socket, outgoingMessage));
        return Task.WhenAll(tasks);
    }
}
