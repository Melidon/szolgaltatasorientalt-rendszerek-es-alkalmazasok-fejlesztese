using Rest_MM5NOT.Repositories;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<IMoviesRepository, MoviesRepository>();

builder.Services
    .AddControllers()
    .AddXmlSerializerFormatters();

var app = builder.Build();

app.MapControllers();

app.Run();
