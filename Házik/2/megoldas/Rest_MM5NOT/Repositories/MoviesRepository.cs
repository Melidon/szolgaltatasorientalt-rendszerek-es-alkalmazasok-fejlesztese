using Rest_MM5NOT.Models;

namespace Rest_MM5NOT.Repositories;

public class MoviesRepository : IMoviesRepository
{
    private static int _nextId = 0;
    private static readonly Dictionary<int, Movie> _movies = new();

    public Movie? GetMovie(int id)
    {
        return _movies.GetValueOrDefault(id);
    }

    public IEnumerable<KeyValuePair<int, Movie>> ListMovies()
    {
        return _movies;
    }

    public int AddMovie(Movie Movie)
    {
        var id = _nextId++;
        _movies.Add(id, Movie);
        return id;
    }

    public void SetMovie(int id, Movie Movie)
    {
        _movies[id] = Movie;
    }

    public void RemoveMovie(int id)
    {
        _movies.Remove(id);
    }
}
