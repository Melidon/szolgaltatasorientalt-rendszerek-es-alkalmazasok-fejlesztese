using Rest_MM5NOT.Models;

namespace Rest_MM5NOT.Repositories;

public interface IMoviesRepository
{
    // Read
    Movie? GetMovie(int id);
    IEnumerable<KeyValuePair<int, Movie>> ListMovies();

    // Write
    int AddMovie(Movie Movie);
    void SetMovie(int id, Movie Movie);
    void RemoveMovie(int id);
}
