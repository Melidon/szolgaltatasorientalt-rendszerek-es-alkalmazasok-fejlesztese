using Microsoft.AspNetCore.Mvc;
using Rest_MM5NOT.Models;
using Rest_MM5NOT.Repositories;

namespace Rest_MM5NOT.Controllers;

[ApiController]
[Route("Rest_MM5NOT/resources/MovieDatabase/[controller]")]
public class MoviesController : ControllerBase
{
    private readonly IMoviesRepository _moviesRepository;

    public MoviesController(IMoviesRepository moviesRepository)
    {
        _moviesRepository = moviesRepository;
    }

    [HttpGet("{id}")]
    public ActionResult<Movie> GetMovie([FromRoute] int id)
    {
        var movie = _moviesRepository.GetMovie(id);
        if (movie is null)
        {
            return NotFound();
        }
        return Ok(movie);
    }

    [HttpGet]
    public ActionResult<MovieList> ListMovies()
    {
        var movies = new MovieList
        {
            Movies = _moviesRepository.ListMovies().Select(pair => pair.Value).ToList(),
        };
        return Ok(movies);
    }

    [HttpPost]
    public ActionResult<IdResult> AddMovie([FromBody] Movie movie)
    {
        var id = _moviesRepository.AddMovie(movie);
        return Ok(new IdResult { Id = id });
    }

    [HttpPut("{id}")]
    public ActionResult SetMovie([FromRoute] int id, [FromBody] Movie movie)
    {
        _moviesRepository.SetMovie(id, movie);
        return NoContent();
    }

    [HttpDelete("{id}")]
    public ActionResult RemoveMovie([FromRoute] int id)
    {
        _moviesRepository.RemoveMovie(id);
        return NoContent();
    }

    [HttpGet("find")]
    public ActionResult<IdList> FindMovies([FromQuery(Name = "year")] int year, [FromQuery(Name = "orderby")] string field)
    {
        var ids = new IdList
        {
            Ids = _moviesRepository
                .ListMovies()
                .Where(pair => pair.Value.Year == year)
                .OrderBy(pair =>
                {
                    switch (field)
                    {
                        case "Title":
                            return pair.Value.Title;
                        case "Director":
                            return pair.Value.Director;
                        default:
                            return string.Empty;
                    }
                })
                .Select(pair => pair.Key)
                .ToList(),
        };
        return Ok(ids);
    }
}
