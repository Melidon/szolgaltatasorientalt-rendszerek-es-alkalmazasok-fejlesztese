using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Rest_MM5NOT.Converters;

public class DataContractJsonConverter<T> : JsonConverter<T>
{
    private readonly DataContractJsonSerializer _serializer = new DataContractJsonSerializer(typeof(T));

    public override T? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        using var jsonDoc = JsonDocument.ParseValue(ref reader);
        var jsonString = jsonDoc.RootElement.GetRawText();
        var jsonBytes = Encoding.UTF8.GetBytes(jsonString);
        using var memoryStream = new MemoryStream(jsonBytes);
        return (T?)_serializer.ReadObject(memoryStream);
    }

    public override void Write(Utf8JsonWriter writer, T value, JsonSerializerOptions options)
    {
        using var memoryStream = new MemoryStream();
        _serializer.WriteObject(memoryStream, value);
        var jsonBytes = memoryStream.ToArray();
        var jsonString = Encoding.UTF8.GetString(jsonBytes);
        var jsonDoc = JsonDocument.Parse(jsonString);
        jsonDoc.WriteTo(writer);
    }
}
