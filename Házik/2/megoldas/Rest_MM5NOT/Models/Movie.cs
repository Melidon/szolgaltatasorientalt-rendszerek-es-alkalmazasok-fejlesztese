using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace Rest_MM5NOT.Models;

[XmlRoot("movie")]
public record Movie
{
    [JsonPropertyName("title")]
    [XmlElement("title")]
    public string Title { get; init; } = string.Empty;

    [JsonPropertyName("year")]
    [XmlElement("year")]
    public int Year { get; init; }

    [JsonPropertyName("director")]
    [XmlElement("director")]
    public string Director { get; init; } = string.Empty;

    [JsonPropertyName("actor")]
    [XmlElement("actor")]
    public List<string> Actors { get; init; } = new();
}

[XmlRoot("movies")]
public record MovieList
{
    [JsonPropertyName("movie")]
    [XmlElement("movie")]
    public List<Movie> Movies { get; init; } = new();
}

[XmlRoot("movies")]
public record IdList
{
    [JsonPropertyName("id")]
    [XmlElement("id")]
    public List<int> Ids { get; init; } = new();
}

[XmlRoot("result")]
public record IdResult
{
    [JsonPropertyName("id")]
    [XmlElement("id")]
    public int Id { get; init; }
}
