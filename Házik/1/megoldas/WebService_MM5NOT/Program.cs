using Microsoft.Extensions.DependencyInjection.Extensions;
using SeatReservation;
using SoapCore;

var builder = WebApplication.CreateBuilder(args);

// Register the SoapCore library:
builder.Services.AddSoapCore();

// Register our Cinema service implementing the ICinema interface:
builder.Services.TryAddSingleton<ICinema, Cinema>();
var app = builder.Build();

// Specify the endpoint of our Cinema service:
app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.UseSoapEndpoint<ICinema>("/WebService_MM5NOT/Cinema", new SoapEncoderOptions(),
    SoapSerializer.DataContractSerializer);
});

app.Run();
