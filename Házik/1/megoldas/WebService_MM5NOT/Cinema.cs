using System.ServiceModel;

namespace SeatReservation;

internal class SeatEqualityComparer : IEqualityComparer<Seat>
{
    public bool Equals(Seat? x, Seat? y)
    {
        if (object.ReferenceEquals(x, y))
            return true;

        if (object.ReferenceEquals(x, null) || object.ReferenceEquals(y, null))
            return false;

        return x.Row == y.Row && x.Column == y.Column;
    }

    public int GetHashCode(Seat obj)
    {
        if (object.ReferenceEquals(obj, null))
            return 0;

        int hashRow = obj.Row == null ? 0 : obj.Row.GetHashCode();
        int hashColumn = obj.Column == null ? 0 : obj.Column.GetHashCode();
        return hashRow ^ hashColumn;
    }
}

public class Cinema : ICinema
{
    private static Dictionary<Seat, SeatStatus> seats_ = new Dictionary<Seat, SeatStatus>(new SeatEqualityComparer());
    private static Dictionary<string, Tuple<Seat, int>> locks_ = new Dictionary<string, Tuple<Seat, int>>();

    public Task BuyAsync(string lockId)
    {
        Tuple<Seat, int> tuple;
        var success = locks_.TryGetValue(lockId, out tuple!);
        if (!success)
        {
            // ha nincs ilyen azonosítójú zárolás, akkor CinemaException-t kell dobni
            throw new FaultException<CinemaException>(new CinemaException()
            {
                ErrorCode = 7,
                ErrorMessage = "ha nincs ilyen azonosítójú zárolás, akkor CinemaException-t kell dobni"
            });
        }
        var (seat, count) = tuple;
        foreach (var column in Enumerable.Range(Int32.Parse(seat.Column), count))
        {
            seats_[new Seat() { Row = seat.Row, Column = $"{column}" }] = SeatStatus.Sold;
        }
        locks_.Remove(lockId);
        return Task.CompletedTask;
    }

    // visszaadja a moziteremben lévő székeket
    // a sorokat az angol ABC nagy betűi jelölik ‘A’-tól kezdve sorfolytonosan
    // az oszlopokat egész számok jelölik, 1-től kezdve sorfolytonosan
    public Task<Seat[]> GetAllSeatsAsync()
    {
        var result = seats_.Keys.ToArray();
        return Task.FromResult(result);
    }

    // megadja, hogy mi az adott helyen lévő szék státusza (szabad, zárolt, foglalt vagy eladva)
    public Task<SeatStatus> GetSeatStatusAsync(Seat seat)
    {
        SeatStatus seatStatus;
        var success = seats_.TryGetValue(seat, out seatStatus);
        if (!success)
        {
            // ha a megadott szék pozíciója hibás, akkor CinemaException-t kell dobni
            throw new FaultException<CinemaException>(new CinemaException()
            {
                ErrorCode = 3,
                ErrorMessage = "ha a megadott szék pozíciója hibás, akkor CinemaException-t kell dobni"
            });
        }
        return Task.FromResult(seatStatus);
    }

    // inicializálja a mozitermet a megadott számú sorokkal és oszlopokkal
    public Task InitAsync(int rows, int columns)
    {
        // a sorok száma: 1 <= rows <= 26
        if (!(1 <= rows && rows <= 26))
        {
            // ha a sorok vagy oszlopok száma kívül esik a fent megadott tartományon, akkor CinemaException-t kell dobni
            throw new FaultException<CinemaException>(new CinemaException()
            {
                ErrorCode = 1,
                ErrorMessage = "a sorok száma: 1 <= rows <= 26"
            });
        }
        // az oszlopok száma: 1 <= columns <= 100
        if (!(1 <= columns && columns <= 100))
        {
            // ha a sorok vagy oszlopok száma kívül esik a fent megadott tartományon, akkor CinemaException-t kell dobni
            throw new FaultException<CinemaException>(new CinemaException()
            {
                ErrorCode = 2,
                ErrorMessage = "az oszlopok száma: 1 <= columns <= 100"
            });
        }
        // minden szék szabad, és minden korábbi zárolás, foglalás törlődik
        seats_.Clear();
        foreach (var row in Enumerable.Range(1, rows))
        {
            foreach (var column in Enumerable.Range(1, columns))
            {
                var seat = new Seat()
                {
                    Row = $"{(char)('A' - 1 + row)}",
                    Column = $"{column}"
                };
                seats_.Add(seat, SeatStatus.Free);
            }
        }
        return Task.CompletedTask;
    }

    // az adott széktől kezdve az adott sorban count darab folytonosan egybefüggő székeket
    // zárol előrefelé számolva
    public Task<string> LockAsync(Seat seat, int count)
    {
        foreach (var column in Enumerable.Range(Int32.Parse(seat.Column), count))
        {
            SeatStatus seatStatus;
            var success = seats_.TryGetValue(new Seat() { Row = seat.Row, Column = $"{column}" }, out seatStatus);
            if (!success || seatStatus != SeatStatus.Free)
            {
                // ha a zárolás valamilyen okból nem teljesíthető (pl. nincs annyi maradék szék a sorban,
                // vagy az egybefüggő széksorozatból nem mindegyik szék szabad), akkor
                // CinemaException-t kell dobni, és nem szabad semmit zárolni
                throw new FaultException<CinemaException>(new CinemaException()
                {
                    ErrorCode = 4,
                    ErrorMessage = "ha a zárolás valamilyen okból nem teljesíthető (pl. nincs annyi maradék szék a sorban, vagy az egybefüggő széksorozatból nem mindegyik szék szabad), akkor CinemaException-t kell dobni, és nem szabad semmit zárolni"
                });
            }
        }
        foreach (var column in Enumerable.Range(Int32.Parse(seat.Column), count))
        {
            seats_[new Seat() { Row = seat.Row, Column = $"{column}" }] = SeatStatus.Locked;
        }
        var lockId = Guid.NewGuid().ToString();
        // a függvénynek vissza kell adnia egy egyedi azonosítót, ami alapján vissza tudja keresni a
        // zárolt székeket
        locks_.Add(lockId, new Tuple<Seat, int>(seat, count));
        return Task.FromResult(lockId);
    }

    // lefoglalja az adott azonosítójú zárolást, és minden a zároláshoz tartozó szék foglalt lesz
    public Task ReserveAsync(string lockId)
    {
        Tuple<Seat, int> tuple;
        var success = locks_.TryGetValue(lockId, out tuple!);
        if (!success)
        {
            // ha nincs ilyen azonosítójú zárolás, akkor CinemaException-t kell dobni
            throw new FaultException<CinemaException>(new CinemaException()
            {
                ErrorCode = 6,
                ErrorMessage = "ha nincs ilyen azonosítójú zárolás, akkor CinemaException-t kell dobni"
            });
        }
        var (seat, count) = tuple;
        foreach (var column in Enumerable.Range(Int32.Parse(seat.Column), count))
        {
            seats_[new Seat() { Row = seat.Row, Column = $"{column}" }] = SeatStatus.Reserved;
        }
        locks_.Remove(lockId);
        return Task.CompletedTask;
    }

    // feloldja az adott azonosítójú zárolást, és minden a zároláshoz tartozó szék szabad lesz
    public Task UnlockAsync(string lockId)
    {
        Tuple<Seat, int> tuple;
        var success = locks_.TryGetValue(lockId, out tuple!);
        if (!success)
        {
            // ha nincs ilyen azonosítójú zárolás, akkor CinemaException-t kell dobni
            throw new FaultException<CinemaException>(new CinemaException()
            {
                ErrorCode = 5,
                ErrorMessage = "ha nincs ilyen azonosítójú zárolás, akkor CinemaException-t kell dobni"
            });
        }
        var (seat, count) = tuple;
        foreach (var column in Enumerable.Range(Int32.Parse(seat.Column), count))
        {
            var status = seats_[new Seat() { Row = seat.Row, Column = $"{column}" }];
            // az Unlock foglalást nem old fel, csak zárolást
            if (status == SeatStatus.Locked)
            {
                seats_[new Seat() { Row = seat.Row, Column = $"{column}" }] = SeatStatus.Free;
            }
        }
        locks_.Remove(lockId);
        return Task.CompletedTask;
    }
}
