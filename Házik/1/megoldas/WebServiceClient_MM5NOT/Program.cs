using System.ServiceModel;
using SeatReservation;

CinemaClient? cinema = null;
try
{
    var url = args[0];
    var row = args[1];
    var column = args[2];
    var task = args[3];

    cinema = new CinemaClient(new BasicHttpBinding(), new EndpointAddress(new Uri(url)));

    switch (task)
    {
        case "Lock":
            {
                var lockId = await cinema.LockAsync(new Seat() { Row = row, Column = column }, 1);
            }
            break;
        case "Reserve":
            {
                var lockId = await cinema.LockAsync(new Seat() { Row = row, Column = column }, 1);
                await cinema.ReserveAsync(lockId);
            }
            break;
        case "Buy":
            {
                var lockId = await cinema.LockAsync(new Seat() { Row = row, Column = column }, 1);
                await cinema.BuyAsync(lockId);
            }
            break;
        default:
            {
            }
            break;
    }
}
finally
{
    if (cinema is not null)
    {
        cinema.Close();
    }
}
