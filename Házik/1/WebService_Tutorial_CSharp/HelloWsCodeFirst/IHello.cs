using System.ServiceModel;
namespace HelloWsCodeFirst
{
    [ServiceContract]
    public interface IHello
    {
        [OperationContract]
        string SayHello(string name);
    }
}
