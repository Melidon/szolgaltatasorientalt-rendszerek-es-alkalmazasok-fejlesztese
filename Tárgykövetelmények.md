# Tárgykövetelmények

## Aláírás, megajánlott jegy

Az aláírás megszerzésének feltétele legalább 3 házi feladat elfogadása. A megajánlott jegyhez mind az 5 házi feladatnak elfogadásra kell kerülnie. A megajánlott jegy az 5 db házi feladat pontszámának átlagából számítódik kerekítéssel (.5-től felfelé).

Egy házi feladat akkor számít elfogadottnak, ha legalább 2.00 pontszámot sikerült elérni.

## Pótlás

Ha a félév során nem sikerül 3 feladatot legalább 2.00 pontszámmal teljesíteni, a házi feladatok a pótlási héten pótolhatók, így még lehet teljesíteni az aláírás követelményeit.

Ha a félév során mind az 5 feladat érdemi tartalommal beadásra került, de nem sikerült mindet legalább 2.00 pontszámmal teljesíteni, vagy a megajánlott jegy nem a kívánt értékű, a házi feladatok a pótlási héten javíthatók. A javításként beadott eredmény lesz figyelembe véve, így lehet javítani vagy akár rontani is a megajánlott jegyen, de aláírás semmiképpen nem veszik el. (Ha azonban félév közben nem került beadásra mind az 5 feladat, akkor pótlással már nem lehet megajánlott jegyet szerezni.)

## Vizsga

Aki a megajánlott jegyet elfogadja, az vegye fel valamelyik vizsgát a Neptunban, és arra be lesz írva az eredmény. Aki szerzett aláírást, de megajánlott jegyet nem, annak vizsgáznia kell. Aki nem kéri a megajánlott jegyet, annak is vizsgáznia kell.
